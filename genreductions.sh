# Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
# This file is a part of the project CES, licensed under the MIT license.
# See LICENSE.md for the full text of the license.
# 
# The above notice shall be included in all copies or substantial 
# portions of this file.
SRC=srcreductions
rm -r $SRC
mkdir $SRC

python openmpeigenbench_reductions.py -r 0.1  -n 8 -b 4  > ./$SRC/trd01n8b4.c
python openmpeigenbench_reductions.py -r 0.2  -n 8 -b 4  > ./$SRC/trd02n8b4.c
python openmpeigenbench_reductions.py -r 0.3  -n 8 -b 4  > ./$SRC/trd03n8b4.c
python openmpeigenbench_reductions.py -r 0.5  -n 8 -b 4  > ./$SRC/trd05n8b4.c
python openmpeigenbench_reductions.py -r 0.7  -n 8 -b 4  > ./$SRC/trd07n8b4.c
python openmpeigenbench_reductions.py -r 0.9  -n 8 -b 4  > ./$SRC/trd09n8b4.c
python openmpeigenbench_reductions.py -r 1  -n 8 -b 4  > ./$SRC/trd1n8b4.c
python openmpeigenbench_reductions.py -r 1.5  -n 8 -b 4  > ./$SRC/trd1.5n8b4.c
python openmpeigenbench_reductions.py -r 2  -n 8 -b 4  > ./$SRC/trd2n8b4.c
python openmpeigenbench_reductions.py -r 2.5  -n 8 -b 4  > ./$SRC/trd25n8b4.c
python openmpeigenbench_reductions.py -r 5  -n 8 -b 4  > ./$SRC/trd5n8b4.c
python openmpeigenbench_reductions.py -r 10  -n 8 -b 4  > ./$SRC/trd10n8b4.c
python openmpeigenbench_reductions.py -r 15  -n 8 -b 4  > ./$SRC/trd15n8b4.c
python openmpeigenbench_reductions.py -r 20  -n 8 -b 4  > ./$SRC/trd20n8b4.c
python openmpeigenbench_reductions.py -r 25  -n 8 -b 4  > ./$SRC/trd25n8b4.c
python openmpeigenbench_reductions.py -r 30  -n 8 -b 4  > ./$SRC/trd30n8b4.c
python openmpeigenbench_reductions.py -r 35  -n 8 -b 4  > ./$SRC/trd35n8b4.c


python openmpeigenbench_reductions.py -r 0.1  -n 4 -b 4  > ./$SRC/trd01n4b4.c
python openmpeigenbench_reductions.py -r 0.2  -n 4 -b 4  > ./$SRC/trd02n4b4.c
python openmpeigenbench_reductions.py -r 0.3  -n 4 -b 4  > ./$SRC/trd03n4b4.c
python openmpeigenbench_reductions.py -r 0.5  -n 4 -b 4  > ./$SRC/trd05n4b4.c
python openmpeigenbench_reductions.py -r 0.7  -n 4 -b 4  > ./$SRC/trd07n4b4.c
python openmpeigenbench_reductions.py -r 0.9  -n 4 -b 4  > ./$SRC/trd09n4b4.c
python openmpeigenbench_reductions.py -r 1  -n 4 -b 4  > ./$SRC/trd1n4b4.c
python openmpeigenbench_reductions.py -r 1.5  -n 4 -b 4  > ./$SRC/trd1.5n4b4.c
python openmpeigenbench_reductions.py -r 2  -n 4 -b 4  > ./$SRC/trd2n4b4.c
python openmpeigenbench_reductions.py -r 2.5  -n 4 -b 4  > ./$SRC/trd25n4b4.c
python openmpeigenbench_reductions.py -r 5  -n 4 -b 4  > ./$SRC/trd5n4b4.c
python openmpeigenbench_reductions.py -r 10  -n 4 -b 4  > ./$SRC/trd10n4b4.c
python openmpeigenbench_reductions.py -r 15  -n 4 -b 4  > ./$SRC/trd15n4b4.c
python openmpeigenbench_reductions.py -r 20  -n 4 -b 4  > ./$SRC/trd20n4b4.c
python openmpeigenbench_reductions.py -r 25  -n 4 -b 4  > ./$SRC/trd25n4b4.c
python openmpeigenbench_reductions.py -r 30  -n 4 -b 4  > ./$SRC/trd30n4b4.c
python openmpeigenbench_reductions.py -r 35  -n 4 -b 4  > ./$SRC/trd35n4b4.c
python openmpeigenbench_reductions.py -r 30  -n 4 -b 4  > ./$SRC/trd30n4b4.c



python openmpeigenbench_reductions.py -r 0.1  -n 4 -b 0 > ./$SRC/trd01n4b0.c
python openmpeigenbench_reductions.py -r 0.2  -n 4 -b 0 > ./$SRC/trd02n4b0.c
python openmpeigenbench_reductions.py -r 0.3  -n 4 -b 0 > ./$SRC/trd03n4b0.c
python openmpeigenbench_reductions.py -r 0.5  -n 4 -b 0 > ./$SRC/trd05n4b0.c
python openmpeigenbench_reductions.py -r 0.7  -n 4 -b 0 > ./$SRC/trd07n4b0.c
python openmpeigenbench_reductions.py -r 0.9  -n 4 -b 0 > ./$SRC/trd09n4b0.c
python openmpeigenbench_reductions.py -r 1  -n 4 -b 0 > ./$SRC/trd1n4b0.c
python openmpeigenbench_reductions.py -r 1.5  -n 4 -b 0 > ./$SRC/trd1.5n4b0.c
python openmpeigenbench_reductions.py -r 2  -n 4 -b 0 > ./$SRC/trd2n4b0.c
python openmpeigenbench_reductions.py -r 2.5  -n 4 -b 0 > ./$SRC/trd25n4b0.c
python openmpeigenbench_reductions.py -r 5  -n 4 -b 0 > ./$SRC/trd5n4b0.c
python openmpeigenbench_reductions.py -r 10  -n 4 -b 0 > ./$SRC/trd10n4b0.c
python openmpeigenbench_reductions.py -r 15  -n 4 -b 0 > ./$SRC/trd15n4b0.c
python openmpeigenbench_reductions.py -r 20  -n 4 -b 0 > ./$SRC/trd20n4b0.c
python openmpeigenbench_reductions.py -r 25  -n 4 -b 0 > ./$SRC/trd25n4b0.c
python openmpeigenbench_reductions.py -r 30  -n 4 -b 0 > ./$SRC/trd30n4b0.c
python openmpeigenbench_reductions.py -r 35  -n 4 -b 0 > ./$SRC/trd35n4b0.c



python openmpeigenbench_reductions.py -r 0.1  -n 6 -b 2 > ./$SRC/trd01n6b2.c
python openmpeigenbench_reductions.py -r 0.2  -n 6 -b 2 > ./$SRC/trd02n6b2.c
python openmpeigenbench_reductions.py -r 0.3  -n 6 -b 2 > ./$SRC/trd03n6b2.c
python openmpeigenbench_reductions.py -r 0.5  -n 6 -b 2 > ./$SRC/trd05n6b2.c
python openmpeigenbench_reductions.py -r 0.7  -n 6 -b 2 > ./$SRC/trd07n6b2.c
python openmpeigenbench_reductions.py -r 0.9  -n 6 -b 2 > ./$SRC/trd09n6b2.c
python openmpeigenbench_reductions.py -r 1  -n 6 -b 2 > ./$SRC/trd1n6b2.c
python openmpeigenbench_reductions.py -r 1.5  -n 6 -b 2 > ./$SRC/trd1.5n6b2.c
python openmpeigenbench_reductions.py -r 2  -n 6 -b 2 > ./$SRC/trd2n6b2.c
python openmpeigenbench_reductions.py -r 2.5  -n 6 -b 2 > ./$SRC/trd25n6b2.c
python openmpeigenbench_reductions.py -r 5  -n 6 -b 2 > ./$SRC/trd5n6b2.c
python openmpeigenbench_reductions.py -r 10  -n 6 -b 2 > ./$SRC/trd10n6b2.c
python openmpeigenbench_reductions.py -r 15  -n 6 -b 2 > ./$SRC/trd15n6b2.c
python openmpeigenbench_reductions.py -r 20  -n 6 -b 2 > ./$SRC/trd20n6b2.c
python openmpeigenbench_reductions.py -r 25  -n 6 -b 2 > ./$SRC/trd25n6b2.c
python openmpeigenbench_reductions.py -r 30  -n 6 -b 2 > ./$SRC/trd30n6b2.c
python openmpeigenbench_reductions.py -r 35  -n 6 -b 2 > ./$SRC/trd35n6b2.c

