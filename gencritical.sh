# Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
# This file is a part of the project CES, licensed under the MIT license.
# See LICENSE.md for the full text of the license.
# 
# The above notice shall be included in all copies or substantial 
# portions of this file.
SRC=srccritical
rm -r $SRC
mkdir $SRC

python openmpeigenbench_critical.py -c 1  -n 4 -b 4  > ./$SRC/tcr1n4b4.c
python openmpeigenbench_critical.py -c 2  -n 4 -b 4  > ./$SRC/tcr2n4b4.c
python openmpeigenbench_critical.py -c 3  -n 4 -b 4  > ./$SRC/tcr3n4b4.c
python openmpeigenbench_critical.py -c 5  -n 4 -b 4  > ./$SRC/tcr5n4b4.c
python openmpeigenbench_critical.py -c 10  -n 4 -b 4  > ./$SRC/tcr10n4b4.c
python openmpeigenbench_critical.py -c 20  -n 4 -b 4  > ./$SRC/tcr20n4b4.c
python openmpeigenbench_critical.py -c 25  -n 4 -b 4  > ./$SRC/tcr25n4b4.c
python openmpeigenbench_critical.py -c 50  -n 4 -b 4  > ./$SRC/tcr50n4b4.c
python openmpeigenbench_critical.py -c 75  -n 4 -b 4  > ./$SRC/tcr75n4b4.c
python openmpeigenbench_critical.py -c 100  -n 4 -b 4  > ./$SRC/tcr100n4b4.c
python openmpeigenbench_critical.py -c 200  -n 4 -b 4  > ./$SRC/tcr200n4b4.c
python openmpeigenbench_critical.py -c 250  -n 4 -b 4  > ./$SRC/tcr250n4b4.c
python openmpeigenbench_critical.py -c 500  -n 4 -b 4  > ./$SRC/tcr500n4b4.c
python openmpeigenbench_critical.py -c 600  -n 4 -b 4  > ./$SRC/tcr600n4b4.c
python openmpeigenbench_critical.py -c 750  -n 4 -b 4  > ./$SRC/tcr750n4b4.c
python openmpeigenbench_critical.py -c 1000  -n 4 -b 4  > ./$SRC/tcr1000n4b4.c
python openmpeigenbench_critical.py -c 2000  -n 4 -b 4  > ./$SRC/tcr2000n4b4.c
python openmpeigenbench_critical.py -c 4000  -n 4 -b 4  > ./$SRC/tcr4000n4b4.c
python openmpeigenbench_critical.py -c 8000  -n 4 -b 4  > ./$SRC/tcr8000n4b4.c
python openmpeigenbench_critical.py -c 16000  -n 4 -b 4  > ./$SRC/tcr16000n4b4.c
python openmpeigenbench_critical.py -c 32000  -n 4 -b 4  > ./$SRC/tcr32000n4b4.c
python openmpeigenbench_critical.py -c 64000  -n 4 -b 4  > ./$SRC/tcr64000n4b4.c
python openmpeigenbench_critical.py -c 128000  -n 4 -b 4  > ./$SRC/tcr128000n4b4.c


python openmpeigenbench_critical.py -c 1  -n 4 -b 4  > ./$SRC/1tcr1n4b4.c
python openmpeigenbench_critical.py -c 2  -n 4 -b 4  > ./$SRC/1tcr2n4b4.c
python openmpeigenbench_critical.py -c 3  -n 4 -b 4  > ./$SRC/1tcr3n4b4.c
python openmpeigenbench_critical.py -c 5  -n 4 -b 4  > ./$SRC/1tcr5n4b4.c
python openmpeigenbench_critical.py -c 10  -n 4 -b 4  > ./$SRC/1tcr10n4b4.c
python openmpeigenbench_critical.py -c 20  -n 4 -b 4  > ./$SRC/1tcr20n4b4.c
python openmpeigenbench_critical.py -c 25  -n 4 -b 4  > ./$SRC/1tcr25n4b4.c
python openmpeigenbench_critical.py -c 50  -n 4 -b 4  > ./$SRC/1tcr50n4b4.c
python openmpeigenbench_critical.py -c 75  -n 4 -b 4  > ./$SRC/1tcr75n4b4.c
python openmpeigenbench_critical.py -c 100  -n 4 -b 4  > ./$SRC/1tcr100n4b4.c
python openmpeigenbench_critical.py -c 200  -n 4 -b 4  > ./$SRC/1tcr200n4b4.c
python openmpeigenbench_critical.py -c 250  -n 4 -b 4  > ./$SRC/1tcr250n4b4.c
python openmpeigenbench_critical.py -c 500  -n 4 -b 4  > ./$SRC/1tcr500n4b4.c
python openmpeigenbench_critical.py -c 600  -n 4 -b 4  > ./$SRC/1tcr600n4b4.c
python openmpeigenbench_critical.py -c 750  -n 4 -b 4  > ./$SRC/1tcr750n4b4.c
python openmpeigenbench_critical.py -c 1000  -n 4 -b 4  > ./$SRC/1tcr1000n4b4.c
python openmpeigenbench_critical.py -c 2000  -n 4 -b 4  > ./$SRC/1tcr2000n4b4.c
python openmpeigenbench_critical.py -c 4000  -n 4 -b 4  > ./$SRC/1tcr4000n4b4.c
python openmpeigenbench_critical.py -c 8000  -n 4 -b 4  > ./$SRC/1tcr8000n4b4.c
python openmpeigenbench_critical.py -c 16000  -n 4 -b 4  > ./$SRC/1tcr16000n4b4.c
python openmpeigenbench_critical.py -c 32000  -n 4 -b 4  > ./$SRC/1tcr32000n4b4.c
python openmpeigenbench_critical.py -c 64000  -n 4 -b 4  > ./$SRC/1tcr64000n4b4.c
python openmpeigenbench_critical.py -c 128000  -n 4 -b 4  > ./$SRC/1tcr128000n4b4.c



python openmpeigenbench_critical.py -c 1  -n 4 -b 4  > ./$SRC/2tcr1n4b4.c
python openmpeigenbench_critical.py -c 2  -n 4 -b 4  > ./$SRC/2tcr2n4b4.c
python openmpeigenbench_critical.py -c 3  -n 4 -b 4  > ./$SRC/2tcr3n4b4.c
python openmpeigenbench_critical.py -c 5  -n 4 -b 4  > ./$SRC/2tcr5n4b4.c
python openmpeigenbench_critical.py -c 10  -n 4 -b 4  > ./$SRC/2tcr10n4b4.c
python openmpeigenbench_critical.py -c 20  -n 4 -b 4  > ./$SRC/2tcr20n4b4.c
python openmpeigenbench_critical.py -c 25  -n 4 -b 4  > ./$SRC/2tcr25n4b4.c
python openmpeigenbench_critical.py -c 50  -n 4 -b 4  > ./$SRC/2tcr50n4b4.c
python openmpeigenbench_critical.py -c 75  -n 4 -b 4  > ./$SRC/2tcr75n4b4.c
python openmpeigenbench_critical.py -c 100  -n 4 -b 4  > ./$SRC/2tcr100n4b4.c
python openmpeigenbench_critical.py -c 200  -n 4 -b 4  > ./$SRC/2tcr200n4b4.c
python openmpeigenbench_critical.py -c 250  -n 4 -b 4  > ./$SRC/2tcr250n4b4.c
python openmpeigenbench_critical.py -c 500  -n 4 -b 4  > ./$SRC/2tcr500n4b4.c
python openmpeigenbench_critical.py -c 600  -n 4 -b 4  > ./$SRC/2tcr600n4b4.c
python openmpeigenbench_critical.py -c 750  -n 4 -b 4  > ./$SRC/2tcr750n4b4.c
python openmpeigenbench_critical.py -c 1000  -n 4 -b 4  > ./$SRC/2tcr1000n4b4.c
python openmpeigenbench_critical.py -c 2000  -n 4 -b 4  > ./$SRC/2tcr2000n4b4.c
python openmpeigenbench_critical.py -c 4000  -n 4 -b 4  > ./$SRC/2tcr4000n4b4.c
python openmpeigenbench_critical.py -c 8000  -n 4 -b 4  > ./$SRC/2tcr8000n4b4.c
python openmpeigenbench_critical.py -c 16000  -n 4 -b 4  > ./$SRC/2tcr16000n4b4.c
python openmpeigenbench_critical.py -c 32000  -n 4 -b 4  > ./$SRC/2tcr32000n4b4.c
python openmpeigenbench_critical.py -c 64000  -n 4 -b 4  > ./$SRC/2tcr64000n4b4.c
python openmpeigenbench_critical.py -c 128000  -n 4 -b 4 > ./$SRC/2tcr128000n4b4.c

