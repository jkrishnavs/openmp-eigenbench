# Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
# This file is a part of the project CES, licensed under the MIT license.
# See LICENSE.md for the full text of the license.
# 
# The above notice shall be included in all copies or substantial 
# portions of this file.
SRC=srcfalsestride
rm -r $SRC
mkdir $SRC

python openmpeigenbench_falsememstride.py -r 1  -n 8 -b 4  > ./$SRC/tstrid1n8b4.c
python openmpeigenbench_falsememstride.py -r 2  -n 8 -b 4  > ./$SRC/tstrid2n8b4.c
python openmpeigenbench_falsememstride.py -r 3  -n 8 -b 4  > ./$SRC/tstrid3n8b4.c
python openmpeigenbench_falsememstride.py -r 4  -n 8 -b 4  > ./$SRC/tstrid4n8b4.c



python openmpeigenbench_falsememstride.py -r 1  -n 4 -b 0  > ./$SRC/tstrid1n4b0.c
python openmpeigenbench_falsememstride.py -r 2  -n 4 -b 0  > ./$SRC/tstrid2n4b0.c
python openmpeigenbench_falsememstride.py -r 3  -n 4 -b 0  > ./$SRC/tstrid3n4b0.c
python openmpeigenbench_falsememstride.py -r 4  -n 4 -b 0  > ./$SRC/tstrid4n4b0.c


python openmpeigenbench_falsememstride.py -r 1  -n 4 -b 4  > ./$SRC/tstrid1n4b4.c
python openmpeigenbench_falsememstride.py -r 2  -n 4 -b 4  > ./$SRC/tstrid2n4b4.c
python openmpeigenbench_falsememstride.py -r 3  -n 4 -b 4  > ./$SRC/tstrid3n4b4.c
python openmpeigenbench_falsememstride.py -r 4  -n 4 -b 4  > ./$SRC/tstrid4n4b4.c


python openmpeigenbench_falsememstride.py -r 1  -n 6 -b 2  > ./$SRC/tstrid1n6b2.c
python openmpeigenbench_falsememstride.py -r 2  -n 6 -b 2  > ./$SRC/tstrid2n6b2.c
python openmpeigenbench_falsememstride.py -r 3  -n 6 -b 2  > ./$SRC/tstrid3n6b2.c
python openmpeigenbench_falsememstride.py -r 4  -n 6 -b 2  > ./$SRC/tstrid4n6b2.c

