# Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
# This file is a part of the project CES, licensed under the MIT license.
# See LICENSE.md for the full text of the license.
# 
# The above notice shall be included in all copies or substantial 
# portions of this file.
SRC=srcflush
rm -r $SRC
mkdir $SRC

python openmpeigenbench_flush.py -f 0.10  -n 8 -b 4  > ./$SRC/tflus10n8b4.c
python openmpeigenbench_flush.py -f 0.20  -n 8 -b 4  > ./$SRC/tflus20n8b4.c
python openmpeigenbench_flush.py -f 0.30  -n 8 -b 4  > ./$SRC/tflus30n8b4.c
python openmpeigenbench_flush.py -f 0.40  -n 8 -b 4  > ./$SRC/tflus40n8b4.c
python openmpeigenbench_flush.py -f 0.50  -n 8 -b 4  > ./$SRC/tflus50n8b4.c
python openmpeigenbench_flush.py -f 0.60  -n 8 -b 4  > ./$SRC/tflus60n8b4.c
python openmpeigenbench_flush.py -f 0.70  -n 8 -b 4  > ./$SRC/tflus70n8b4.c
python openmpeigenbench_flush.py -f 0.80  -n 8 -b 4  > ./$SRC/tflus80n8b4.c
python openmpeigenbench_flush.py -f 0.90  -n 8 -b 4  > ./$SRC/tflus90n8b4.c
python openmpeigenbench_flush.py -f 1.00  -n 8 -b 4  > ./$SRC/tflus100n8b4.c
python openmpeigenbench_flush.py -f 2.00  -n 8 -b 4  > ./$SRC/tflus200n8b4.c
python openmpeigenbench_flush.py -f 3.00  -n 8 -b 4  > ./$SRC/tflus300n8b4.c
python openmpeigenbench_flush.py -f 4.00  -n 8 -b 4  > ./$SRC/tflus400n8b4.c
python openmpeigenbench_flush.py -f 5.00  -n 8 -b 4  > ./$SRC/tflus500n8b4.c
python openmpeigenbench_flush.py -f 7.00  -n 8 -b 4  > ./$SRC/tflus700n8b4.c
python openmpeigenbench_flush.py -f 9.00  -n 8 -b 4  > ./$SRC/tflus900n8b4.c


python openmpeigenbench_flush.py -f 0.10  -n 4 -b 4  > ./$SRC/tflus10n4b4.c
python openmpeigenbench_flush.py -f 0.20  -n 4 -b 4  > ./$SRC/tflus20n4b4.c
python openmpeigenbench_flush.py -f 0.30  -n 4 -b 4  > ./$SRC/tflus30n4b4.c
python openmpeigenbench_flush.py -f 0.40  -n 4 -b 4  > ./$SRC/tflus40n4b4.c
python openmpeigenbench_flush.py -f 0.50  -n 4 -b 4  > ./$SRC/tflus50n4b4.c
python openmpeigenbench_flush.py -f 0.60  -n 4 -b 4  > ./$SRC/tflus60n4b4.c
python openmpeigenbench_flush.py -f 0.70  -n 4 -b 4  > ./$SRC/tflus70n4b4.c
python openmpeigenbench_flush.py -f 0.80  -n 4 -b 4  > ./$SRC/tflus80n4b4.c
python openmpeigenbench_flush.py -f 0.90  -n 4 -b 4  > ./$SRC/tflus90n4b4.c
python openmpeigenbench_flush.py -f 1.00  -n 4 -b 4  > ./$SRC/tflus100n4b4.c
python openmpeigenbench_flush.py -f 2.00  -n 4 -b 4  > ./$SRC/tflus200n4b4.c
python openmpeigenbench_flush.py -f 3.00  -n 4 -b 4  > ./$SRC/tflus300n4b4.c
python openmpeigenbench_flush.py -f 4.00  -n 4 -b 4  > ./$SRC/tflus400n4b4.c
python openmpeigenbench_flush.py -f 5.00  -n 4 -b 4  > ./$SRC/tflus500n4b4.c
python openmpeigenbench_flush.py -f 7.00  -n 4 -b 4  > ./$SRC/tflus700n4b4.c
python openmpeigenbench_flush.py -f 9.00  -n 4 -b 4  > ./$SRC/tflus900n4b4.c




python openmpeigenbench_flush.py -f 0.10  -n 4 -b 0  > ./$SRC/tflus10n4b0.c
python openmpeigenbench_flush.py -f 0.20  -n 4 -b 0  > ./$SRC/tflus20n4b0.c
python openmpeigenbench_flush.py -f 0.30  -n 4 -b 0  > ./$SRC/tflus30n4b0.c
python openmpeigenbench_flush.py -f 0.40  -n 4 -b 0  > ./$SRC/tflus40n4b0.c
python openmpeigenbench_flush.py -f 0.50  -n 4 -b 0  > ./$SRC/tflus50n4b0.c
python openmpeigenbench_flush.py -f 0.60  -n 4 -b 0  > ./$SRC/tflus60n4b0.c
python openmpeigenbench_flush.py -f 0.70  -n 4 -b 0  > ./$SRC/tflus70n4b0.c
python openmpeigenbench_flush.py -f 0.80  -n 4 -b 0  > ./$SRC/tflus80n4b0.c
python openmpeigenbench_flush.py -f 0.90  -n 4 -b 0  > ./$SRC/tflus90n4b0.c
python openmpeigenbench_flush.py -f 1.00  -n 4 -b 0  > ./$SRC/tflus100n4b0.c
python openmpeigenbench_flush.py -f 2.00  -n 4 -b 0  > ./$SRC/tflus200n4b0.c
python openmpeigenbench_flush.py -f 3.00  -n 4 -b 0  > ./$SRC/tflus300n4b0.c
python openmpeigenbench_flush.py -f 4.00  -n 4 -b 0  > ./$SRC/tflus400n4b0.c
python openmpeigenbench_flush.py -f 5.00  -n 4 -b 0  > ./$SRC/tflus500n4b0.c
python openmpeigenbench_flush.py -f 7.00  -n 4 -b 0  > ./$SRC/tflus700n4b0.c
python openmpeigenbench_flush.py -f 9.00  -n 4 -b 0  > ./$SRC/tflus900n4b0.c


python openmpeigenbench_flush.py -f 0.10  -n 6 -b 2  > ./$SRC/tflus10n6b2.c
python openmpeigenbench_flush.py -f 0.20  -n 6 -b 2  > ./$SRC/tflus20n6b2.c
python openmpeigenbench_flush.py -f 0.30  -n 6 -b 2  > ./$SRC/tflus30n6b2.c
python openmpeigenbench_flush.py -f 0.40  -n 6 -b 2  > ./$SRC/tflus40n6b2.c
python openmpeigenbench_flush.py -f 0.50  -n 6 -b 2  > ./$SRC/tflus50n6b2.c
python openmpeigenbench_flush.py -f 0.60  -n 6 -b 2  > ./$SRC/tflus60n6b2.c
python openmpeigenbench_flush.py -f 0.70  -n 6 -b 2  > ./$SRC/tflus70n6b2.c
python openmpeigenbench_flush.py -f 0.80  -n 6 -b 2  > ./$SRC/tflus80n6b2.c
python openmpeigenbench_flush.py -f 0.90  -n 6 -b 2  > ./$SRC/tflus90n6b2.c
python openmpeigenbench_flush.py -f 1.00  -n 6 -b 2  > ./$SRC/tflus100n6b2.c
python openmpeigenbench_flush.py -f 2.00  -n 6 -b 2  > ./$SRC/tflus200n6b2.c
python openmpeigenbench_flush.py -f 3.00  -n 6 -b 2  > ./$SRC/tflus300n6b2.c
python openmpeigenbench_flush.py -f 4.00  -n 6 -b 2  > ./$SRC/tflus400n6b2.c
python openmpeigenbench_flush.py -f 5.00  -n 6 -b 2  > ./$SRC/tflus500n6b2.c
python openmpeigenbench_flush.py -f 7.00  -n 6 -b 2  > ./$SRC/tflus700n6b2.c
python openmpeigenbench_flush.py -f 9.00  -n 6 -b 2  > ./$SRC/tflus900n6b2.c




