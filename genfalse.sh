# Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
# This file is a part of the project CES, licensed under the MIT license.
# See LICENSE.md for the full text of the license.
# 
# The above notice shall be included in all copies or substantial 
# portions of this file.
SRC=srcfalse
rm -r $SRC
mkdir $SRC

python openmpeigenbench_falsesharing.py -f 1  -n 8 -b 4  -r 1 > ./$SRC/tsr1fal1n8b4.c
python openmpeigenbench_falsesharing.py -f 2  -n 8 -b 4  -r 1 > ./$SRC/tsr1fal2n8b4.c
python openmpeigenbench_falsesharing.py -f 3  -n 8 -b 4  -r 1 > ./$SRC/tsr1fal3n8b4.c
python openmpeigenbench_falsesharing.py -f 5  -n 8 -b 4  -r 1 > ./$SRC/tsr1fal5n8b4.c
python openmpeigenbench_falsesharing.py -f 10  -n 8 -b 4 -r 1 > ./$SRC/tsr1fal10n8b4.c
python openmpeigenbench_falsesharing.py -f 15  -n 8 -b 4 -r 1 > ./$SRC/tsr1fal15n8b4.c
python openmpeigenbench_falsesharing.py -f 20  -n 8 -b 4 -r 1 > ./$SRC/tsr1fal20n8b4.c
python openmpeigenbench_falsesharing.py -f 25  -n 8 -b 4 -r 1 > ./$SRC/tsr1fal25n8b4.c


python openmpeigenbench_falsesharing.py -f 1  -n 6 -b 2  -r 1 > ./$SRC/tsr1fal1n6b2.c
python openmpeigenbench_falsesharing.py -f 2  -n 6 -b 2  -r 1 > ./$SRC/tsr1fal2n6b2.c
python openmpeigenbench_falsesharing.py -f 3  -n 6 -b 2  -r 1 > ./$SRC/tsr1fal3n6b2.c
python openmpeigenbench_falsesharing.py -f 5  -n 6 -b 2  -r 1 > ./$SRC/tsr1fal5n6b2.c
python openmpeigenbench_falsesharing.py -f 10  -n 6 -b 2 -r 1 > ./$SRC/tsr1fal10n6b2.c
python openmpeigenbench_falsesharing.py -f 15  -n 6 -b 2 -r 1 > ./$SRC/tsr1fal15n6b2.c
python openmpeigenbench_falsesharing.py -f 20  -n 6 -b 2 -r 1 > ./$SRC/tsr1fal20n6b2.c
python openmpeigenbench_falsesharing.py -f 25  -n 6 -b 2 -r 1 > ./$SRC/tsr1fal25n6b2.c

python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 4  -r 1 > ./$SRC/tsr1fal1n4b4.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 4  -r 1 > ./$SRC/tsr1fal2n4b4.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 4  -r 1 > ./$SRC/tsr1fal3n4b4.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 4  -r 1 > ./$SRC/tsr1fal5n4b4.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 4 -r 1 > ./$SRC/tsr1fal10n4b4.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 4 -r 1 > ./$SRC/tsr1fal15n4b4.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 4 -r 1 > ./$SRC/tsr1fal20n4b4.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 4 -r 1 > ./$SRC/tsr1fal25n4b4.c


python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 0  -r 1 > ./$SRC/tsr1fal1n4b0.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 0  -r 1 > ./$SRC/tsr1fal2n4b0.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 0  -r 1 > ./$SRC/tsr1fal3n4b0.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 0  -r 1 > ./$SRC/tsr1fal5n4b0.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 0 -r 1 > ./$SRC/tsr1fal10n4b0.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 0 -r 1 > ./$SRC/tsr1fal15n4b0.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 0 -r 1 > ./$SRC/tsr1fal20n4b0.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 0 -r 1 > ./$SRC/tsr1fal25n4b0.c

python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 2  -r 1 > ./$SRC/tsr1fal1n4b2.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 2  -r 1 > ./$SRC/tsr1fal2n4b2.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 2  -r 1 > ./$SRC/tsr1fal3n4b2.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 2  -r 1 > ./$SRC/tsr1fal5n4b2.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 2 -r 1 > ./$SRC/tsr1fal10n4b2.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 2 -r 1 > ./$SRC/tsr1fal15n4b2.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 2 -r 1 > ./$SRC/tsr1fal20n4b2.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 2 -r 1 > ./$SRC/tsr1fal25n4b2.c

# stride 2

python openmpeigenbench_falsesharing.py -f 1  -n 8 -b 4  -r 2 > ./$SRC/tsr2fal1n8b4.c
python openmpeigenbench_falsesharing.py -f 2  -n 8 -b 4  -r 2 > ./$SRC/tsr2fal2n8b4.c
python openmpeigenbench_falsesharing.py -f 3  -n 8 -b 4  -r 2 > ./$SRC/tsr2fal3n8b4.c
python openmpeigenbench_falsesharing.py -f 5  -n 8 -b 4  -r 2 > ./$SRC/tsr2fal5n8b4.c
python openmpeigenbench_falsesharing.py -f 10  -n 8 -b 4 -r 2 > ./$SRC/tsr2fal10n8b4.c
python openmpeigenbench_falsesharing.py -f 15  -n 8 -b 4 -r 2 > ./$SRC/tsr2fal15n8b4.c
python openmpeigenbench_falsesharing.py -f 20  -n 8 -b 4 -r 2 > ./$SRC/tsr2fal20n8b4.c
python openmpeigenbench_falsesharing.py -f 25  -n 8 -b 4 -r 2 > ./$SRC/tsr2fal25n8b4.c


python openmpeigenbench_falsesharing.py -f 1  -n 6 -b 2  -r 2 > ./$SRC/tsr2fal1n6b2.c
python openmpeigenbench_falsesharing.py -f 2  -n 6 -b 2  -r 2 > ./$SRC/tsr2fal2n6b2.c
python openmpeigenbench_falsesharing.py -f 3  -n 6 -b 2  -r 2 > ./$SRC/tsr2fal3n6b2.c
python openmpeigenbench_falsesharing.py -f 5  -n 6 -b 2  -r 2 > ./$SRC/tsr2fal5n6b2.c
python openmpeigenbench_falsesharing.py -f 10  -n 6 -b 2 -r 2 > ./$SRC/tsr2fal10n6b2.c
python openmpeigenbench_falsesharing.py -f 15  -n 6 -b 2 -r 2 > ./$SRC/tsr2fal15n6b2.c
python openmpeigenbench_falsesharing.py -f 20  -n 6 -b 2 -r 2 > ./$SRC/tsr2fal20n6b2.c
python openmpeigenbench_falsesharing.py -f 25  -n 6 -b 2 -r 2 > ./$SRC/tsr2fal25n6b2.c


python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 4  -r 2 > ./$SRC/tsr2fal1n4b4.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 4  -r 2 > ./$SRC/tsr2fal2n4b4.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 4  -r 2 > ./$SRC/tsr2fal3n4b4.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 4  -r 2 > ./$SRC/tsr2fal5n4b4.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 4 -r 2 > ./$SRC/tsr2fal10n4b4.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 4 -r 2 > ./$SRC/tsr2fal15n4b4.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 4 -r 2 > ./$SRC/tsr2fal20n4b4.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 4 -r 2 > ./$SRC/tsr2fal25n4b4.c


python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 0  -r 2 > ./$SRC/tsr2fal1n4b0.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 0  -r 2 > ./$SRC/tsr2fal2n4b0.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 0  -r 2 > ./$SRC/tsr2fal3n4b0.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 0  -r 2 > ./$SRC/tsr2fal5n4b0.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 0 -r 2 > ./$SRC/tsr2fal10n4b0.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 0 -r 2 > ./$SRC/tsr2fal15n4b0.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 0 -r 2 > ./$SRC/tsr2fal20n4b0.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 0 -r 2 > ./$SRC/tsr2fal25n4b0.c

python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 2  -r 2 > ./$SRC/tsr2fal1n4b2.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 2  -r 2 > ./$SRC/tsr2fal2n4b2.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 2  -r 2 > ./$SRC/tsr2fal3n4b2.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 2  -r 2 > ./$SRC/tsr2fal5n4b2.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 2 -r 2 > ./$SRC/tsr2fal10n4b2.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 2 -r 2 > ./$SRC/tsr2fal15n4b2.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 2 -r 2 > ./$SRC/tsr2fal20n4b2.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 2 -r 2 > ./$SRC/tsr2fal25n4b2.c

#stride 4

python openmpeigenbench_falsesharing.py -f 1  -n 8 -b 4  -r 4 > ./$SRC/tsr4fal1n8b4.c
python openmpeigenbench_falsesharing.py -f 2  -n 8 -b 4  -r 4 > ./$SRC/tsr4fal2n8b4.c
python openmpeigenbench_falsesharing.py -f 3  -n 8 -b 4  -r 4 > ./$SRC/tsr4fal3n8b4.c
python openmpeigenbench_falsesharing.py -f 5  -n 8 -b 4  -r 4 > ./$SRC/tsr4fal5n8b4.c
python openmpeigenbench_falsesharing.py -f 10  -n 8 -b 4 -r 4 > ./$SRC/tsr4fal10n8b4.c
python openmpeigenbench_falsesharing.py -f 15  -n 8 -b 4 -r 4 > ./$SRC/tsr4fal15n8b4.c
python openmpeigenbench_falsesharing.py -f 20  -n 8 -b 4 -r 4 > ./$SRC/tsr4fal20n8b4.c
python openmpeigenbench_falsesharing.py -f 25  -n 8 -b 4 -r 4 > ./$SRC/tsr4fal25n8b4.c

python openmpeigenbench_falsesharing.py -f 1  -n 6 -b 2  -r 4 > ./$SRC/tsr4fal1n6b2.c
python openmpeigenbench_falsesharing.py -f 2  -n 6 -b 2  -r 4 > ./$SRC/tsr4fal2n6b2.c
python openmpeigenbench_falsesharing.py -f 3  -n 6 -b 2  -r 4 > ./$SRC/tsr4fal3n6b2.c
python openmpeigenbench_falsesharing.py -f 5  -n 6 -b 2  -r 4 > ./$SRC/tsr4fal5n6b2.c
python openmpeigenbench_falsesharing.py -f 10  -n 6 -b 2 -r 4 > ./$SRC/tsr4fal10n6b2.c
python openmpeigenbench_falsesharing.py -f 15  -n 6 -b 2 -r 4 > ./$SRC/tsr4fal15n6b2.c
python openmpeigenbench_falsesharing.py -f 20  -n 6 -b 2 -r 4 > ./$SRC/tsr4fal20n6b2.c
python openmpeigenbench_falsesharing.py -f 25  -n 6 -b 2 -r 4 > ./$SRC/tsr4fal25n6b2.c


python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 4  -r 4 > ./$SRC/tsr4fal1n4b4.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 4  -r 4 > ./$SRC/tsr4fal2n4b4.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 4  -r 4 > ./$SRC/tsr4fal3n4b4.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 4  -r 4 > ./$SRC/tsr4fal5n4b4.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 4 -r 4 > ./$SRC/tsr4fal10n4b4.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 4 -r 4 > ./$SRC/tsr4fal15n4b4.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 4 -r 4 > ./$SRC/tsr4fal20n4b4.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 4 -r 4 > ./$SRC/tsr4fal25n4b4.c


python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 0  -r 4 > ./$SRC/tsr4fal1n4b0.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 0  -r 4 > ./$SRC/tsr4fal2n4b0.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 0  -r 4 > ./$SRC/tsr4fal3n4b0.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 0  -r 4 > ./$SRC/tsr4fal5n4b0.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 0 -r 4 > ./$SRC/tsr4fal10n4b0.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 0 -r 4 > ./$SRC/tsr4fal15n4b0.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 0 -r 4 > ./$SRC/tsr4fal20n4b0.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 0 -r 4 > ./$SRC/tsr4fal25n4b0.c


python openmpeigenbench_falsesharing.py -f 1  -n 4 -b 2  -r 4 > ./$SRC/tsr4fal1n4b2.c
python openmpeigenbench_falsesharing.py -f 2  -n 4 -b 2  -r 4 > ./$SRC/tsr4fal2n4b2.c
python openmpeigenbench_falsesharing.py -f 3  -n 4 -b 2  -r 4 > ./$SRC/tsr4fal3n4b2.c
python openmpeigenbench_falsesharing.py -f 5  -n 4 -b 2  -r 4 > ./$SRC/tsr4fal5n4b2.c
python openmpeigenbench_falsesharing.py -f 10  -n 4 -b 2 -r 4 > ./$SRC/tsr4fal10n4b2.c
python openmpeigenbench_falsesharing.py -f 15  -n 4 -b 2 -r 4 > ./$SRC/tsr4fal15n4b2.c
python openmpeigenbench_falsesharing.py -f 20  -n 4 -b 2 -r 4 > ./$SRC/tsr4fal20n4b2.c
python openmpeigenbench_falsesharing.py -f 25  -n 4 -b 2 -r 4 > ./$SRC/tsr4fal25n4b2.c

