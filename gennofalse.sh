# Copyright (c) 2018 Jyothi Krishna V.S., Rupesh Nasre, Shankar Balachandran, IIT Madras.
# This file is a part of the project CES, licensed under the MIT license.
# See LICENSE.md for the full text of the license.
# 
# The above notice shall be included in all copies or substantial 
# portions of this file.
SRC=srcnofalse
rm -r $SRC
mkdir $SRC

python openmpeigenbench_nofalsesharing.py -m 10  -n 8 -b 4  > ./$SRC/tmem10n8b4.c
python openmpeigenbench_nofalsesharing.py -m 15  -n 8 -b 4  > ./$SRC/tmem15n8b4.c
python openmpeigenbench_nofalsesharing.py -m 20  -n 8 -b 4  > ./$SRC/tmem20n8b4.c
python openmpeigenbench_nofalsesharing.py -m 25  -n 8 -b 4  > ./$SRC/tmem25n8b4.c
python openmpeigenbench_nofalsesharing.py -m 30  -n 8 -b 4  > ./$SRC/tmem30n8b4.c
python openmpeigenbench_nofalsesharing.py -m 35  -n 8 -b 4  > ./$SRC/tmem35n8b4.c
python openmpeigenbench_nofalsesharing.py -m 30  -n 8 -b 4  > ./$SRC/tmem30n8b4.c
python openmpeigenbench_nofalsesharing.py -m 40  -n 8 -b 4  > ./$SRC/tmem40n8b4.c
python openmpeigenbench_nofalsesharing.py -m 45  -n 8 -b 4  > ./$SRC/tmem45n8b4.c
python openmpeigenbench_nofalsesharing.py -m 50  -n 8 -b 4  > ./$SRC/tmem50n8b4.c
python openmpeigenbench_nofalsesharing.py -m 55  -n 8 -b 4  > ./$SRC/tmem55n8b4.c
python openmpeigenbench_nofalsesharing.py -m 60  -n 8 -b 4  > ./$SRC/tmem60n8b4.c
python openmpeigenbench_nofalsesharing.py -m 65  -n 8 -b 4  > ./$SRC/tmem65n8b4.c
python openmpeigenbench_nofalsesharing.py -m 70  -n 8 -b 4  > ./$SRC/tmem70n8b4.c
python openmpeigenbench_nofalsesharing.py -m 75  -n 8 -b 4  > ./$SRC/tmem75n8b4.c
python openmpeigenbench_nofalsesharing.py -m 80  -n 8 -b 4  > ./$SRC/tmem80n8b4.c
python openmpeigenbench_nofalsesharing.py -m 90  -n 8 -b 4  > ./$SRC/tmem90n8b4.c
python openmpeigenbench_nofalsesharing.py -m 100  -n 8 -b 4  > ./$SRC/tmem100n8b4.c




python openmpeigenbench_nofalsesharing.py -m 10  -n 4 -b 4  > ./$SRC/tmem10n4b4.c
python openmpeigenbench_nofalsesharing.py -m 15  -n 4 -b 4  > ./$SRC/tmem15n4b4.c
python openmpeigenbench_nofalsesharing.py -m 20  -n 4 -b 4  > ./$SRC/tmem20n4b4.c
python openmpeigenbench_nofalsesharing.py -m 25  -n 4 -b 4  > ./$SRC/tmem25n4b4.c
python openmpeigenbench_nofalsesharing.py -m 30  -n 4 -b 4  > ./$SRC/tmem30n4b4.c
python openmpeigenbench_nofalsesharing.py -m 35  -n 4 -b 4  > ./$SRC/tmem35n4b4.c
python openmpeigenbench_nofalsesharing.py -m 30  -n 4 -b 4  > ./$SRC/tmem30n4b4.c
python openmpeigenbench_nofalsesharing.py -m 40  -n 4 -b 4  > ./$SRC/tmem40n4b4.c
python openmpeigenbench_nofalsesharing.py -m 45  -n 4 -b 4  > ./$SRC/tmem45n4b4.c
python openmpeigenbench_nofalsesharing.py -m 50  -n 4 -b 4  > ./$SRC/tmem50n4b4.c
python openmpeigenbench_nofalsesharing.py -m 55  -n 4 -b 4  > ./$SRC/tmem55n4b4.c
python openmpeigenbench_nofalsesharing.py -m 60  -n 4 -b 4  > ./$SRC/tmem60n4b4.c
python openmpeigenbench_nofalsesharing.py -m 65  -n 4 -b 4  > ./$SRC/tmem65n4b4.c
python openmpeigenbench_nofalsesharing.py -m 70  -n 4 -b 4  > ./$SRC/tmem70n4b4.c
python openmpeigenbench_nofalsesharing.py -m 75  -n 4 -b 4  > ./$SRC/tmem75n4b4.c
python openmpeigenbench_nofalsesharing.py -m 80  -n 4 -b 4  > ./$SRC/tmem80n4b4.c
python openmpeigenbench_nofalsesharing.py -m 90  -n 4 -b 4  > ./$SRC/tmem90n4b4.c
python openmpeigenbench_nofalsesharing.py -m 100  -n 4 -b 4  > ./$SRC/tmem100n4b4.c



python openmpeigenbench_nofalsesharing.py -m 10  -n 4 -b 0  > ./$SRC/tmem10n4b0.c
python openmpeigenbench_nofalsesharing.py -m 15  -n 4 -b 0  > ./$SRC/tmem15n4b0.c
python openmpeigenbench_nofalsesharing.py -m 20  -n 4 -b 0  > ./$SRC/tmem20n4b0.c
python openmpeigenbench_nofalsesharing.py -m 25  -n 4 -b 0  > ./$SRC/tmem25n4b0.c
python openmpeigenbench_nofalsesharing.py -m 30  -n 4 -b 0  > ./$SRC/tmem30n4b0.c
python openmpeigenbench_nofalsesharing.py -m 35  -n 4 -b 0  > ./$SRC/tmem35n4b0.c
python openmpeigenbench_nofalsesharing.py -m 30  -n 4 -b 0  > ./$SRC/tmem30n4b0.c
python openmpeigenbench_nofalsesharing.py -m 40  -n 4 -b 0  > ./$SRC/tmem40n4b0.c
python openmpeigenbench_nofalsesharing.py -m 45  -n 4 -b 0  > ./$SRC/tmem45n4b0.c
python openmpeigenbench_nofalsesharing.py -m 50  -n 4 -b 0  > ./$SRC/tmem50n4b0.c
python openmpeigenbench_nofalsesharing.py -m 55  -n 4 -b 0  > ./$SRC/tmem55n4b0.c
python openmpeigenbench_nofalsesharing.py -m 60  -n 4 -b 0  > ./$SRC/tmem60n4b0.c
python openmpeigenbench_nofalsesharing.py -m 65  -n 4 -b 0  > ./$SRC/tmem65n4b0.c
python openmpeigenbench_nofalsesharing.py -m 70  -n 4 -b 0  > ./$SRC/tmem70n4b0.c
python openmpeigenbench_nofalsesharing.py -m 75  -n 4 -b 0  > ./$SRC/tmem75n4b0.c
python openmpeigenbench_nofalsesharing.py -m 80  -n 4 -b 0  > ./$SRC/tmem80n4b0.c
python openmpeigenbench_nofalsesharing.py -m 90  -n 4 -b 0  > ./$SRC/tmem90n4b0.c
python openmpeigenbench_nofalsesharing.py -m 100  -n 4 -b 0  > ./$SRC/tmem100n4b0.c



python openmpeigenbench_nofalsesharing.py -m 10  -n 6 -b 2  > ./$SRC/tmem10n6b2.c
python openmpeigenbench_nofalsesharing.py -m 15  -n 6 -b 2  > ./$SRC/tmem15n6b2.c
python openmpeigenbench_nofalsesharing.py -m 20  -n 6 -b 2  > ./$SRC/tmem20n6b2.c
python openmpeigenbench_nofalsesharing.py -m 25  -n 6 -b 2  > ./$SRC/tmem25n6b2.c
python openmpeigenbench_nofalsesharing.py -m 30  -n 6 -b 2  > ./$SRC/tmem30n6b2.c
python openmpeigenbench_nofalsesharing.py -m 35  -n 6 -b 2  > ./$SRC/tmem35n6b2.c
python openmpeigenbench_nofalsesharing.py -m 30  -n 6 -b 2  > ./$SRC/tmem30n6b2.c
python openmpeigenbench_nofalsesharing.py -m 40  -n 6 -b 2  > ./$SRC/tmem40n6b2.c
python openmpeigenbench_nofalsesharing.py -m 45  -n 6 -b 2  > ./$SRC/tmem45n6b2.c
python openmpeigenbench_nofalsesharing.py -m 50  -n 6 -b 2  > ./$SRC/tmem50n6b2.c
python openmpeigenbench_nofalsesharing.py -m 55  -n 6 -b 2  > ./$SRC/tmem55n6b2.c
python openmpeigenbench_nofalsesharing.py -m 60  -n 6 -b 2  > ./$SRC/tmem60n6b2.c
python openmpeigenbench_nofalsesharing.py -m 65  -n 6 -b 2  > ./$SRC/tmem65n6b2.c
python openmpeigenbench_nofalsesharing.py -m 70  -n 6 -b 2  > ./$SRC/tmem70n6b2.c
python openmpeigenbench_nofalsesharing.py -m 75  -n 6 -b 2  > ./$SRC/tmem75n6b2.c
python openmpeigenbench_nofalsesharing.py -m 80  -n 6 -b 2  > ./$SRC/tmem80n6b2.c
python openmpeigenbench_nofalsesharing.py -m 90  -n 6 -b 2  > ./$SRC/tmem90n6b2.c
python openmpeigenbench_nofalsesharing.py -m 100  -n 6 -b 2  > ./$SRC/tmem100n6b2.c

